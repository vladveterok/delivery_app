class PackagesController < ApplicationController
  before_action :set_package, only: [:show, :edit, :update, :destroy, :toggle]
 
  # GET /packages
  # GET /packages.json
  def index
    @packages = Package.all
  end

  # GET /packages/1
  # GET /packages/1.json
  def show
    @courier = @package.courier
  end

  # GET /packages/new
  def new
    @package = Package.new
  end

  
  # GET /packages/1/edit
  def edit
    @courier = @package.courier
  end

  # POST /packages
  # POST /packages.json
  def create
    @package = Package.new(package_params)
    @courier = @package.courier

    respond_to do |format|
      if @package.save
        format.html { redirect_to @courier, notice: 'Package was successfully created.' }
        format.json { render :show, status: :created, location: @package }
      else
        format.html { render :new }
        format.json { render json: @package.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /packages/1
  # PATCH/PUT /packages/1.json
  def update
    respond_to do |format|
      if @package.update(package_params)
        format.html { redirect_to @package, notice: 'Package was successfully updated.' }
        format.json { render :show, status: :ok, location: @package }
      else
        format.html { render :edit }
        format.json { render json: @package.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /packages/1
  # DELETE /packages/1.json
  def destroy
    @courier = @package.courier
    @package.destroy
    respond_to do |format|
      format.html { redirect_to @courier, notice: 'Package was successfully destroyed.' }
      format.js
      format.json { head :no_content }
    end
  end

  def toggle
    @package.toggle_completed!
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_package
      @package = Package.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def package_params
      params.require(:package).permit(:name, :tracking_number, :delivery_status, :courier_id)
    end
end
