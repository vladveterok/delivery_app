json.extract! package, :id, :name, :tracking_number, :delivery_status, :courier_id, :created_at, :updated_at
json.url package_url(package, format: :json)
