class Package < ApplicationRecord
  belongs_to :courier

  scope :completed, -> { where(delivery_status: true) }
  scope :uncompleted, -> { where(delivery_status: false) }

  def toggle_completed!
    toggle!(:delivery_status)
  end
end
