Rails.application.routes.draw do
  resources :packages do
    post :toggle, on: :member
  end
  resources :couriers
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root :to => redirect('/couriers')
end
