# Delivery App

This is a simple, tiny, impractical app where you're free to create couriers and assign packages. And edit. And delete. And that's pretty much it. :)

## Requirements
- Ruby 2.7.0
- Rails '~> 6.0.2', '>= 6.0.2.1'
- Database: PostgreSQL 12.2

If you're still considering switching from sqlite3 to PostgreSQL, here's a simple yet comprehensive guide on how to set up and use PostgreSQL with Rails on OS X: https://gist.github.com/migzsuelto/3826180

## Installation


To get the initial code please clone your fork of this repo to your machine and execute the following command in your top-level projects directory:

```bash
$ git clone https://github.com/vladveterok/delivery_app.git
```

Once you have the clone of the repo:

1. Change into the actual project directory: ```cd delivery_app ```

2. Run ```bundle install``` to make sure all gems are properly installed.
3. Run

```bash
bundle exec rake db:migrate
```
to apply database migrations.

4. Run ```bin/rails s```
5. Go to ```http://localhost:3000/```
6. Have an enjoyable day. :)