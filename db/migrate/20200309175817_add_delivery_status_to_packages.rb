class AddDeliveryStatusToPackages < ActiveRecord::Migration[6.0]
  def change
    add_column :packages, :delivery_status, :boolean, null: false, default: false
  end
end
