class RemoveDeliveryStatusFromPackages < ActiveRecord::Migration[6.0]
  def change
    remove_column :packages, :delivery_status
  end
end
